// Project: Manuk 
// Created: 2019-03-20

// show all errors
SetErrorMode(2)

// set window properties
SetWindowTitle( "Manuk" )
SetWindowSize( 640, 360 ,0 )
SetWindowAllowResize( 0 ) // allow the user to resize the window

// set display properties
SetVirtualResolution( 640, 360 ) // doesn't have to match the window
SetOrientationAllowed(1,1,0,0 ) // allow both portrait and landscape on mobile devices
SetSyncRate( 30, 0 ) // 30fps instead of 60 to save battery
SetScissor( 0,0,0,0 ) // use the maximum available screen space, no black borders
UseNewDefaultFonts( 1 ) // since version 2.0.22 we can use nicer default fonts

#include "resource.agc"
#include "player.agc"
#include "enemy.agc"
#include "coin.agc"

GoSub resource
GoSub player
GoSub enemy

moveX = -40
moveY = GetVirtualHeight()-GetSpriteHeight(kcMeneng)-70
ambil = 1
harta = 0
score = 0
emati = 0
dogX = GetVirtualWidth()+50
dogY = GetVirtualHeight()-GetSpriteHeight(dogJalan)-70
nembak = 0
bolaX = 0
bolaY = GetVirtualHeight()-(GetSpriteHeight(kcMeneng)-40)



CreateText(1,"Koin :")
SetTextPosition(1,550,1)
SetTextSize(1,22)
CreateText(2,str(harta))
SetTextSize(2,22)
SetTextPosition(2,550+GetTextTotalWidth(1)+5,1)

CreateText(3,"Score :")
SetTextPosition(3,0,1)
SetTextSize(3,22)
CreateText(4,str(score))
SetTextSize(4,22)
SetTextPosition(4,GetTextTotalWidth(3)+5,1)


do
	GoSub coin
	GoSub playergerak
	GoSub enemygerak
	GoSub playersenjata
    Sync()
loop
